# Hell Triangle - Challenge #


### História do Cliente ###
   Dado um triângulo de números, encontre o total máximo de cima para baixo Exemplo:
    6 
   3 5
  9 7 1
 4 6 8 4
 	Neste triângulo o total máximo é: 6 + 5 + 7 + 8 = 26
 Um elemento só pode ser somado com um dos dois elementos mais próximos na próxima linha. Por exemplo: O elemento 3 na 2ª linha só pode ser somado com 9 e 7, mas não com 1.
Seu código receberá uma matriz (multidimensional) como entrada. O triângulo acima seria:
exemplo = [[6], [3,5], [9,7,1], [4,6,8,4]].

### Linguagem escolhida ###
Para essa atividade que não necessita alguma funcionalidade específica, pois é uma funcionalidade voltada para lógica de programação eu irei utilizar linguagem: Javascript. Linguagem essa que roda em qualquer browser e com isso ajudará na hora de executar o código. 
Já que tenho habilidades no front-end e também no back-end  também irei fazer uma tela funcional. 
Essa tela será feita em HTML, CSS. 

### Solução Encontrada ###
Percebendo que havia a necessidade de sempre somar os números da linha de baixo mais próximos, que no meu entendimento seriam os que ficam à esquerda e a direita em relação a linha vertical do número superior.  E que o triangular a cada linha tinha um número a mais que a anterior. Cheguei à conclusão que o índice da linha atual era o primeiro índice mais próximo da linha posterior.  Sendo assim esse índice mais acrescentando mais 1, seria o segundo número mais próximo. 
Após ter os dois números mais próximos, fiz uma condição para saber qual era o maior entre eles e adicionei o mesmo a soma total do triangulo. 

### Como executar o código ###
Para executar, basta baixar o projeto, ter um browser instalado no computador, entrar na pasta raiz e localizar o arquivo "index.html".   
Ao executar esse arquivo o sistema irá mostrar 10 caixas de entrada de texto, alinhado de modo que forme um triangulo. Essas caixas de texto serão preenchidas com números aleatórios de 0 a 9. Após preencher as caixas de texto será executada a função que faz o cálculo do total do triangulo de cima pra baixo seguindo as regras do cliente.
