
$(document).ready(function(){
    TestCalculoTriangulo();
});

var index = new Array();


function mostrarEsconderResultados(mostrar){
    if(mostrar){
        $("#linhaResultado").slideDown(500);
    }else{
        $("#linhaResultado").slideUp(500);
    }
}

function setCoresPadraoCampos(){
    $(".linhas").each(function(indexLinha, e){
        $(".linha" + (indexLinha + 1) + " .form-control").each(function(index, input){
           setCorPretaFundoBranco(input)
        });
    });
}

function setCorPretaFundoBranco(input){
    var corPreta = "#000000";
    var corBranca = "#ffffff";
    $(input).css({backgroundColor: corBranca, color: corPreta});
}

function setNumerosTela(){
    var numeroAleatorio;
    $(".linhas").each(function(indexLinha, e){
        $(".linha" + (indexLinha + 1) + " .form-control").each(function(index, input){
            numeroAleatorio = gerarNumeroAletorioZeroANove();
            $(input).val(numeroAleatorio);
            setCorPretaFundoBranco(input)
        });
    });
    mostrarEsconderResultados(false);
}

function gerarNumeroAletorioZeroANove(){
    return Math.floor(Math.random() * (9)) + 1;
}

function getDadosTela(){
    var quantidadesVazios = 0;
    var valorCampo;
    var corVermelha = "#d9534f"
    var corBranca = "#ffffff";
    var dados = new Array();
    $(".linhas").each(function(indexLinha, e){
        var linha = new Array();
        $(".linha" + (indexLinha + 1) + " .form-control").each(function(index, input){
            valorCampo = $(input).val();
            if(valorCampo){
                linha.push(parseInt($(input).val()));
                setCorPretaFundoBranco(input);
            }else{
                quantidadesVazios++;
                $(input).css({backgroundColor: corVermelha, color: corBranca});
            }
        });
        dados.push(linha);
    });
    if(quantidadesVazios > 0){
        alert("Existe(m) " + quantidadesVazios + " campo(s) em vermelho que precisa(m) ser preenchido(s)!");
    }else{
        var resultado = getResultadoCalcularTriangulo(dados);
        setTotalCaminhoPercorrido(resultado);
    }
}

function setTotalCaminhoPercorrido(resultado){
    var listaValores = new Array();
    var corVerde = "#4cae4c";
    var corBranca = "#ffffff";
    var listaIndexs = resultado["listIndexs"];
    var valorTotal = resultado["total"];
    var indexCampo;
    var resultadoTela = "";
    $(".linhas").each(function(indexLinha, e){
        indexCampo = listaIndexs[indexLinha];
        $(".linha" + (indexLinha + 1) + " .form-control").each(function(index, e){
            if(indexCampo == index){
                if(indexLinha > 0){
                    resultadoTela += " + ";
                }
                resultadoTela += "<b>" + $(e).val() + "</b>";
                $(e).css({backgroundColor: corVerde, color: corBranca});
            }
        });
    });
    resultadoTela += " = <b>" + valorTotal + "</b>";
    $("#resultadoFinal").html(resultadoTela);
    mostrarEsconderResultados(true);
}


function apenasNumeros(input){
    var value = input.value;
    var apenasNumero = /^[0-9]$/g;
    if(!apenasNumero.test(value)){
        input.value = ""
    }else{
        setCorPretaFundoBranco(input);
    }
    setCoresPadraoCampos();
    mostrarEsconderResultados(false);
}

function TestCalculoTriangulo(){
    setNumerosTela();
    getDadosTela();
}

function getResultadoCalcularTriangulo(dados){
    var indexElementoTela = 1;
    var indexValorProximoAnterior = undefined;
    var indexSegundoValorProximo;
    var totalMaxTriangulo = 0;
    var linhaAtual;
    var listaIndexsCalculados = new Array();
    for(var indexLinha in dados){
        linhaAtual = dados[indexLinha];
        console.log(linhaAtual);
        if(indexValorProximoAnterior != undefined){
            indexSegundoValorProximo = parseInt(indexValorProximoAnterior) + 1;
            if(linhaAtual[indexValorProximoAnterior] >= linhaAtual[indexSegundoValorProximo]){
                totalMaxTriangulo = (totalMaxTriangulo + linhaAtual[indexValorProximoAnterior]);
                console.log("primeiro index: " + indexValorProximoAnterior + " valor: " + linhaAtual[indexValorProximoAnterior]);
            }else{
                totalMaxTriangulo = (totalMaxTriangulo + linhaAtual[indexSegundoValorProximo]);
                indexValorProximoAnterior = indexSegundoValorProximo;
                console.log("segundo index: " + indexValorProximoAnterior + " valor: " + linhaAtual[indexSegundoValorProximo]);
            }
        }else{
            totalMaxTriangulo = (totalMaxTriangulo+  linhaAtual[indexLinha]);
            indexValorProximoAnterior = parseInt(indexLinha);
            console.log("Inicio index: " + indexValorProximoAnterior + " valor: " + linhaAtual[indexValorProximoAnterior]);
        }
        listaIndexsCalculados.push(indexValorProximoAnterior);
    }
    return {total: totalMaxTriangulo, listIndexs: listaIndexsCalculados};
}